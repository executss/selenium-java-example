package ua.goodwine.com.pages;

import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class HomePage extends MainPages {
    public HomePage(WebDriver driver) {
        super(driver);
    }
    @Step("Open Main page")
    public void OpenMainPage(int loadPageTime,String massage){
        isPageload(loadPageTime,"Home page not load above " + loadPageTime + " seconds");
    }
}
