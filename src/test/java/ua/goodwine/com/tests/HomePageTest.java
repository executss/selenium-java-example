package ua.goodwine.com.tests;


import io.qameta.allure.*;

import org.openqa.selenium.*;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import ua.goodwine.com.pages.HomePage;
import ua.goodwine.com.pages.MainMenu;


//@Epic("Epic")
//@Feature("Feature")

public class HomePageTest extends MainTest {
    private WebDriver driver;
    private HomePage homePage;
    private MainMenu mainMenu;

    @BeforeMethod(description = "Initialize driver")
    @Issue("ALR-123")
    public void Before(){
        DriverInitialize("https://main-stage.goodwine.com.ua/");
        this.driver = getDriver();
        this.homePage = new HomePage(this.driver);
        this.mainMenu = new MainMenu(this.driver);

    }

    @Test(description = "Open Home page less 10s")
    @Description("TEST Description")
    @Severity(SeverityLevel.CRITICAL)

    public void FirstTest(){
        homePage.OpenMainPage(10,"open Home page less 10 seconds");
    }
    @Test(description = "click on fist element main menu")
    public void SecondTest(){

        mainMenu.ClickOnFirstMenu("click on menu 'Vine'");

    }

    @AfterMethod
    public void AfterCall(){

        attachment();
        driver.close();
    }


}
