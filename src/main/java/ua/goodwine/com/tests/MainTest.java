package ua.goodwine.com.tests;

import io.qameta.allure.Attachment;
import org.openqa.selenium.*;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.Point;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

import static org.testng.AssertJUnit.assertTrue;

public class MainTest {
    private static WebDriver driver;
    private static String driverPath = "./selenium//";
    public static int DefaultDelay = 10;
    public WebDriver getDriver() {
        return driver;
    }


    public void DriverInitialize(String SITE_URL){
        System.setProperty("webdriver.chrome.driver", driverPath + "chromedriver.exe");
        driver = new ChromeDriver();
        getDriver().manage().deleteAllCookies();
//      driver.manage().window().maximize();
        getDriver().manage().window().setPosition(new Point(0, 0));
        getDriver().manage().window().setSize(new Dimension(1366, 768));
        getDriver().manage().timeouts().implicitlyWait(DefaultDelay,TimeUnit.SECONDS);
        getDriver().get(SITE_URL);

    }
    public boolean getElement(By locator){
        try{
            getDriver().findElement(locator);
            return true;
        } catch (NoSuchElementException e){
            return false;
        }
    }

    @Attachment(value = "screenshot", type = "image/png")
    public byte[] attachment()  {
        byte[] src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.BYTES);
        return src;
    }
}
